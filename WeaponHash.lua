Sites intéressants :
https://www.ign.com/wikis/gta-5/Weapon_Customizations

https://wiki.gtanet.work/index.php?title=Weapons_Tints
https://wiki.gtanet.work/index.php?title=Weapons_Models
https://wiki.gtanet.work/index.php?title=Weapons_Components

https://docs.altv.mp/gta/articles/weapons/components.html

Weapons Tints ID https://wiki.gtanet.work/index.php?title=Weapons_Tints

Normal	0
Green	1
Gold	2
Pink	3
Army	4
LSPD	5
Orange	6
Platinum	7


weapon name                          int hash     int hash (hex)         uint hash   uint (hex)
PICKUP_AMMO_BULLET_MP              ,  1426343849,         0x550447a9,    1426343849, 0x550447a9
PICKUP_AMMO_FIREWORK               ,  -114341780, 0xfffffffff92f486c,    4180625516, 0xf92f486c
PICKUP_AMMO_FIREWORK_MP            ,  1613316560,         0x602941d0,    1613316560, 0x602941d0
PICKUP_AMMO_FLAREGUN               ,  -535568356, 0xffffffffe013e01c,    3759398940, 0xe013e01c
PICKUP_AMMO_GRENADELAUNCHER        , -2011516760, 0xffffffff881ab0a8,    2283450536, 0x881ab0a8
PICKUP_AMMO_GRENADELAUNCHER_MP     , -1541298894, 0xffffffffa421a532,    2753668402, 0xa421a532
PICKUP_AMMO_HOMINGLAUNCHER         ,  1548844439,         0x5c517d97,    1548844439, 0x5c517d97
PICKUP_AMMO_MG                     ,  -564600653, 0xffffffffde58e0b3,    3730366643, 0xde58e0b3
PICKUP_AMMO_MINIGUN                ,  -228982343, 0xfffffffff25a01b9,    4065984953, 0xf25a01b9
PICKUP_AMMO_MISSILE_MP             ,  -107080240, 0xfffffffff99e15d0,    4187887056, 0xf99e15d0
PICKUP_AMMO_PISTOL                 ,   544828034,         0x20796a82,     544828034, 0x20796a82
PICKUP_AMMO_RIFLE                  ,  -457363514, 0xffffffffe4bd2fc6,    3837603782, 0xe4bd2fc6
PICKUP_AMMO_RPG                    , -2071756841, 0xffffffff84837fd7,    2223210455, 0x84837fd7
PICKUP_AMMO_SHOTGUN                ,  2012476125,         0x77f3f2dd,    2012476125, 0x77f3f2dd
PICKUP_AMMO_SMG                    ,   292537574,         0x116fc4e6,     292537574, 0x116fc4e6
PICKUP_AMMO_SNIPER                 , -1070796507, 0xffffffffc02cf125,    3224170789, 0xc02cf125

Groupe d\'armes :

--[[
melee: 2685387236
Handguns: 416676503
Submachine Gun: 3337201093
Shotgun: 860033945
Assault Rifle: 970310034
Light Machine Gun: 1159398588
Sniper: 3082541095
Heavy Weapon: 2725924767
Throwables: 1548507267
Misc: 4257178988
]]--

if 0x99B507EA or -1716189206 then "weapon_knife" -- Knife
	elseif 0x678B81B1 or 1737195953 then "weapon_nightstick" -- Nightstick
	elseif 0x4E875F73 or 1317494643 then "weapon_hammer" -- Hammer
	elseif 0x958A4A8F or -1786099057 then "weapon_bat" -- Bat
	elseif 0x440E4788 or 1141786504 then "weapon_golfclub" -- Golfclub
	elseif 0x84BD7BFD or -2067956739 then "weapon_crowbar" -- Crowbar
	elseif 0x1B06D571 or 453432689 then "weapon_pistol" -- Pistol
	elseif 0x5EF9FEC4 or 1593441988 then "weapon_combatpistol" -- CombatPistol
	elseif 0x22D8FE39 or 584646201 then "weapon_appistol" -- APPistol
	elseif 0x99AEEB3B or -1716589765 then "weapon_pistol50" -- Pistol .50
	elseif 0x3656C8C1 or 911657153 then "weapon_stungun" -- StunGun
	elseif 0x13532244 or 324215364 then "weapon_microsmg" -- Micro SMG
	elseif 0x2BE6766B or 736523883 then "weapon_smg" -- SMG
	elseif 0xEFE7E2DF or -270015777 then "weapon_assaultsmg" -- Assault SMG
	elseif 0x0A3D4D34 or 171789620 then "weapon_combatpdw" -- Combat PDW
	elseif 0xBFEFFF6D or -1074790547 then "weapon_assaultrifle" -- Assault Rifle
	elseif 0x83BF0278 or -2084633992 then "weapon_carbinerifle" -- Carbine Rifle
	elseif 0xAF113F99 or -1357824103 then "weapon_advancedrifle" -- Advanced Rifle
	elseif 0x9D07F764 or -1660422300 then "weapon_mg" -- MG
	elseif 0x7FD62962 or 2144741730 then "weapon_combatmg" -- Combat MG
	elseif 0x1D073A89 or 487013001 then "weapon_pumpshotgun" -- PumpShotgun
	elseif 0x7846A318 or 2017895192 then "weapon_sawnoffshotgun" -- Sawed-Off Shotgun
	elseif 0xE284C527 or -494615257 then "weapon_assaultshotgun" -- Assault Shotgun
	elseif 0x9D61E50F or -1654528753 then "weapon_bullpupshotgun" -- Bullpup Shotgun
	elseif 0x5FC3C11 or 100416529 then "weapon_sniperrifle" -- Sniper Rifle
	elseif 0xC472FE2 or 205991906 then "weapon_heavysniper" -- Heavy Sniper
	elseif 0xA284510B or -1568386805 then "weapon_grenadelauncher" -- Grenade Launcher
	elseif 0x4DD2DC56 or 1305664598 then "weapon_grenadelauncher_smoke" -- Grenade Launcher Smoke
	elseif 0xB1CA77B1 or -1312131151 then "weapon_rpg" -- RPG
	elseif 0x42BF8A85 or 1119849093 then "weapon_minigun" -- Minigun
	elseif 0x93E220BD or -1813897027 then "weapon_grenade" -- Grenade
	elseif 0x2C3731D9 or 741814745 then "weapon_stickybomb" -- Sticky Bomb
	elseif 0xFDBC8A50 or -37975472 then "weapon_smokegrenade" -- Tear Gas
	elseif 0xA0973D5E or -1600701090 then "weapon_bzgas" -- BZ Gas
	elseif 0x24B17070 or 615608432 then "weapon_molotov" -- Molotov Cocktail
	elseif 0x60EC506 or 101631238 then "weapon_fireextinguisher" -- Fire Extinguisher
	elseif 0x34A67B97 or 883325847 then "weapon_petrolcan" -- Jerry Can
	elseif 0xBFD21232 or -1076751822 then "weapon_snspistol" -- SNS Pistol
	elseif 0xC0A3098D or -1063057011 then "weapon_specialcarbine" -- Special Carbine
	elseif 0xD205520E or -771403250 then "weapon_heavypistol" -- Heavy Pistol
	elseif 0x7F229F94 or 2132975508 then "weapon_bullpuprifle" -- Bullpup Rifle
	elseif 0x63AB0442 or 1672152130 then "weapon_hominglauncher" -- Homing Launcher
	elseif 0xAB564B93 or -1420407917 then "weapon_proxmine" -- Proximity Mine
	elseif 0x787F0BB or 126349499 then "weapon_snowball" -- Snowballs
	elseif 0x83839C4 or 137902532 then "weapon_vintagepistol" -- Vintage Pistol
	elseif 0x92A27487 or -1834847097 then "weapon_dagger" -- Antique Cavalry Dagger
	elseif 0x7F7497E5 or 2138347493 then "weapon_firework" -- Firework Launcher
	elseif 0xA89CB99E or -1466123874 then "weapon_musket" -- Musket
	elseif 0xC734385A or -952879014 then "weapon_marksmanrifle" -- Marksman Rifle
	elseif 0x3AABBBAA or 984333226 then "weapon_heavyshotgun" -- Heavy Shotgun
	elseif 0x61012683 or 1627465347 then "weapon_gusenberg" -- Gusenberg Sweeper
	elseif 0xF9DCBF2D or -102973651 then "weapon_hatchet" -- Hatchet
	elseif 0x6D544C99 or 1834241177 then "weapon_railgun" -- Railgun
	elseif 0xA2719263 or -1569615261 then "weapon_unarmed" -- Fist
	elseif 0x57A4368C or 1470379660 then "weapon_gadgetpistol" -- Perico Pistol
	elseif 0xF9E6AA4B or -102323637 then "weapon_bottle" -- Broken Bottle
	elseif 0x8BB05FD7 or -1951375401 then "weapon_flashlight" -- Flashlight
	elseif 0xD8DF3C3C or 656458692 then "weapon_knuckle" -- Brass Knuckles
	elseif 0xDD5DF8D9 or -581044007 then "weapon_machete" -- Machete
	elseif 0xDFE37640 or -538741184 then "weapon_switchblade" -- Switchblade
	elseif 0x19044EE0 or 419712736 then "weapon_wrench" -- Pipe Wrench
	elseif 0xCD274149 or -853065399 then "weapon_battleaxe" -- attle Axe
	elseif 0x94117305 or -1810795771 then "weapon_poolcue" -- Pool Cue
	elseif 0x3813FC08 or -102973651 then "weapon_stone_hatchet" -- Stone Hatchet
	elseif 0x88374054 or -2009644972 then "weapon_snspistol_mk2" -- SNS Pistol Mk II
	elseif 0x47757124 or 1198879012 then "weapon_flaregun" -- Flare Gun
	elseif 0xDC4DB296 or -598887786 then "weapon_marksmanpistol" -- Marksman Pistol
	elseif 0xC1B3C3D1 or -1045183535 then "weapon_revolver" -- Heavy Revolver
	elseif 0xCB96392F or -879347409 then "weapon_revolver_mk2" -- Heavy Revolver Mk II
	elseif 0x97EA20B8 or -1746263880 then "weapon_doubleaction" -- Double Action Revolver
	elseif 0xAF3696A1 or -1355376991 then "weapon_raypistol" -- Up-n-Atomizer
	elseif 0x2B5EF5EC or 727643628 then "weapon_ceramicpistol" -- Ceramic Pistol
	elseif 0x917F6C8C or -1853920116 then "weapon_navyrevolver" -- Navy Revolver
	elseif 0x78A97CD0 or 2024373456 then "weapon_smg_mk2" -- SMG Mk II
	elseif 0xDB1AA450 or -619010992 then "weapon_machinepistol" -- Machine Pistol
	elseif 0xBD248B55 or -1121678507 then "weapon_minismg" -- Mini SMG
	elseif 0x476BF155 or 1198256469 then "weapon_raycarbine" -- Unholy Hellbringer
	elseif 0x555AF99A or 1432025498 then "weapon_pumpshotgun_mk2" -- Pump Shotgun Mk II
	elseif 0xEF951FBB or -275439685 then "weapon_dbshotgun" -- Double Barrel Shotgun
	elseif 0x12E82D3D or 317205821 then "weapon_autoshotgun" -- Sweeper Shotgun
	elseif 0x5A96BA4 or 94989220 then "weapon_combatshotgun" -- Combat Shotgun
	elseif 0x394F415C or 961495388 then "weapon_assaultrifle_mk2" -- Assault Rifle Mk II
	elseif 0xFAD1F1C9 or -86904375 then "weapon_carbinerifle_mk2" -- Carbine Rifle Mk II
	elseif 0x969C3D67 or -1768145561 then "weapon_specialcarbine_mk2" -- Special Carbine Mk II
	elseif 0x84D6FAFD or -2066285827 then "weapon_bullpuprifle_mk2" -- Bullpup Rifle Mk II
	elseif 0x624FE830 or 1649403952 then "weapon_compactrifle" -- Compact Rifle
	elseif 0x9D1F17E6 or -1658906650 then "weapon_militaryrifle" -- Military Rifle
	elseif 0xDBBD7280 or -608341376 then "weapon_combatmg_mk2" -- Combat MG Mk II
	elseif 0xA914799 or 177293209 then "weapon_heavysniper_mk2" -- Heavy Sniper Mk II
	elseif 0x6A6C02E0 or 1785463520 then "weapon_marksmanrifle_mk2" -- Marksman Rifle Mk II
	elseif 0x0781FE4A or 125959754 then "weapon_compactlauncher" -- Compact Grenade Launcher
	elseif 0xB62D1F67 or -1238556825 then "weapon_rayminigun" -- Widowmaker
end

const ammoTypeByHash = {  -- GET_PED_AMMO_TYPE_FROM_WEAPON -- https://gist.github.com/shockdevv/ecaf76711c282427bae31449f6c99b3a -- https://gist.github.com/root-cause/faf41f59f7a6d818b7db0b839bd147c1
    '94989220': 2416459067,
    '100416529': 1285032059,
    '101631238': 1359393852,
    '125959754': 1003267566,
    '126349499': 2182627693,
    '137902532': 1950175060,
    '171789620': 1820140472,
    '177293209': 1285032059,
    '205991906': 1285032059,
    '317205821': 2416459067,
    '324215364': 1820140472,
    '419712736': 0,
    '453432689': 1950175060,
    '487013001': 2416459067,
    '584646201': 1950175060,
    '600439132': 4287981158,
    '615608432': 1446246869,
    '727643628': 1950175060,
    '736523883': 1820140472,
    '741814745': 1411692055,
    '911657153': 2955849184,
    '940833800': 0,
    '961495388': 218444191,
    '984333226': 2416459067,
    '1119849093': 2680539266,
    '1141786504': 0,
    '1198256469': 1788949567,
    '1198879012': 1173416293,
    '1233104067': 1808594799,
    '1305664598': 826266432,
    '1317494643': 0,
    '1432025498': 2416459067,
    '1470379660': 1950175060,
    '1593441988': 1950175060,
    '1627465347': 1788949567,
    '1649403952': 218444191,
    '1672152130': 2568293933,
    '1737195953': 0,
    '1785463520': 1285032059,
    '1834241177': 2034517757,
    '2017895192': 2416459067,
    '2024373456': 1820140472,
    '2132975508': 218444191,
    '2138347493': 2938367503,
    '2144741730': 1788949567,
    '2210333304': 218444191,
    '2227010557': 0,
    '2228681469': 218444191,
    '2285322324': 1950175060,
    '2343591895': 0,
    '2441047180': 1950175060,
    '2460120199': 0,
    '2481070269': 1003688881,
    '2484171525': 0,
    '2508868239': 0,
    '2526821735': 218444191,
    '2548703416': 1950175060,
    '2578377531': 1950175060,
    '2578778090': 0,
    '2634544996': 1788949567,
    '2636060646': 218444191,
    '2640438543': 2416459067,
    '2694266206': 2608103076,
    '2725352035': 0,
    '2726580491': 1003267566,
    '2828843422': 2416459067,
    '2874559379': 2938243239,
    '2937143193': 218444191,
    '2939590305': 2768943988,
    '2982836145': 1742569970,
    '3056410471': 2680539266,
    '3125143736': 357983224,
    '3126027122': 1618528319,
    '3173288789': 1820140472,
    '3215233542': 0,
    '3218215474': 1950175060,
    '3219281620': 1950175060,
    '3220176749': 218444191,
    '3231910285': 218444191,
    '3249783761': 1950175060,
    '3342088282': 1285032059,
    '3415619887': 1950175060,
    '3441901897': 0,
    '3523564046': 1950175060,
    '3638508604': 0,
    '3675956304': 1820140472,
    '3686625920': 1788949567,
    '3696079510': 1950175060,
    '3713923289': 0,
    '3756226112': 0,
    '3800352039': 2416459067,
    '4019527611': 2416459067,
    '4024951519': 1820140472,
    '4191993645': 0,
    '4192643659': 0,
    '4208062921': 218444191,
    '4222310262': 0,
    '4256991824': 3859679398
}

public enum WeaponComponent : uint -- https://github.com/crosire/scripthookvdotnet/blob/main/source/scripting_v2/GTA.Native/WeaponComponent.cs
	{
		AdvancedRifleClip01 = 0xFA8FA10F,
		AdvancedRifleClip02 = 0x8EC1C979,
		AdvancedRifleVarmodLuxe = 0x377CD377,
		APPistolClip01 = 0x31C4B22A,
		APPistolClip02 = 0x249A17D5,
		APPistolVarmodLuxe = 0x9B76C72C,
		AssaultRifleClip01 = 0xBE5EEA16,
		AssaultRifleClip02 = 0xB1214F9B,
		AssaultRifleClip03 = 0xDBF0A53D,
		AssaultRifleVarmodLuxe = 0x4EAD7533,
		AssaultRifleMk2Camo = 0x911B24AF,
		AssaultRifleMk2Camo02 = 0x37E5444B,
		AssaultRifleMk2Camo03 = 0x538B7B97,
		AssaultRifleMk2Camo04 = 0x25789F72,
		AssaultRifleMk2Camo05 = 0xC5495F2D,
		AssaultRifleMk2Camo06 = 0xCF8B73B1,
		AssaultRifleMk2Camo07 = 0xA9BB2811,
		AssaultRifleMk2Camo08 = 0xFC674D54,
		AssaultRifleMk2Camo09 = 0x7C7FCD9B,
		AssaultRifleMk2Camo10 = 0xA5C38392,
		AssaultRifleMk2CamoIndependence01 = 0xB9B15DB0,
		AssaultRifleMk2Clip01 = 0x8610343F,
		AssaultRifleMk2Clip02 = 0xD12ACA6F,
		AssaultRifleMk2ClipArmorPiercing = 0xA7DD1E58,
		AssaultRifleMk2ClipFMJ = 0x63E0A098,
		AssaultRifleMk2ClipIncendiary = 0xFB70D853,
		AssaultRifleMk2ClipTracer = 0xEF2C78C1,
		AssaultShotgunClip01 = 0x94E81BC7,
		AssaultShotgunClip02 = 0x86BD7F72,
		AssaultSMGClip01 = 0x8D1307B0,
		AssaultSMGClip02 = 0xBB46E417,
		AssaultSMGVarmodLowrider = 0x278C78AF,
		AtArAfGrip = 0xC164F53,
		AtArAfGrip02 = 0x9D65907A,
		AtArBarrel01 = 0x43A49D26,
		AtArBarrel02 = 0x5646C26A,
		AtArFlsh = 0x7BC4CDDC,
		AtArSupp = 0x837445AA,
		AtArSupp02 = 0xA73D4664,
		AtBpBarrel01 = 0x659AC11B,
		AtBpBarrel02 = 0x3BF26DC7,
		AtCrBarrel01 = 0x833637FF,
		AtCrBarrel02 = 0x8B3C480B,
		AtMGBarrel01 = 0xC34EF234,
		AtMGBarrel02 = 0xB5E2575B,
		AtMrFlBarrel01 = 0x381B5D89,
		AtMrFlBarrel02 = 0x68373DDC,
		AtMuzzle01 = 0xB99402D4,
		AtMuzzle02 = 0xC867A07B,
		AtMuzzle03 = 0xDE11CBCF,
		AtMuzzle04 = 0xEC9068CC,
		AtMuzzle05 = 0x2E7957A,
		AtMuzzle06 = 0x347EF8AC,
		AtMuzzle07 = 0x4DB62ABE,
		AtMuzzle08 = 0x5F7DCE4D,
		AtMuzzle09 = 0x6927E1A1,
		AtPiComp = 0x21E34793,
		AtPiComp02 = 0xAA8283BF,
		AtPiComp03 = 0x27077CCB,
		AtPiFlsh = 0x359B7AAE,
		AtPiFlsh02 = 0x43FD595B,
		AtPiFlsh03 = 0x4A4965F3,
		AtPiRail = 0x8ED4BB70,
		AtPiRail02 = 0x47DE9258,
		AtPiSupp = 0xC304849A,
		AtPiSupp02 = 0x65EA7EBB,
		AtRailCover01 = 0x75414F30,
		AtSbBarrel01 = 0xD9103EE1,
		AtSbBarrel02 = 0xA564D78B,
		AtScBarrel01 = 0xE73653A9,
		AtScBarrel02 = 0xF97F783B,
		AtScopeLarge = 0xD2443DDC,
		AtScopeLargeFixedZoom = 0x1C221B1A,
		AtScopeLargeFixedZoomMk2 = 0x5B1C713C,
		AtScopeLargeMk2 = 0x82C10383,
		AtScopeMacro = 0x9D2FBF29,
		AtScopeMacroMk2 = 0x49B2945,
		AtScopeMacro02 = 0x3CC6BA57,
		AtScopeMacro02Mk2 = 0xC7ADD105,
		AtScopeMacro02SMGMk2 = 0xE502AB6B,
		AtScopeMax = 0xBC54DA77,
		AtScopeMedium = 0xA0D89C42,
		AtScopeMediumMk2 = 0xC66B6542,
		AtScopeNV = 0xB68010B0,
		AtScopeSmall = 0xAA2C45B4,
		AtScopeSmall02 = 0x3C00AFED,
		AtScopeSmallMk2 = 0x3F3C8181,
		AtScopeSmallSMGMk2 = 0x3DECC7DA,
		AtScopeThermal = 0x2E43DA41,
		AtSights = 0x420FD713,
		AtSightsSMG = 0x9FDB5652,
		AtSrBarrel01 = 0x909630B7,
		AtSrBarrel02 = 0x108AB09E,
		AtSrSupp = 0xE608B35E,
		AtSrSupp03 = 0xAC42DF71,
		BullpupRifleClip01 = 0xC5A12F80,
		BullpupRifleClip02 = 0xB3688B0F,
		BullpupRifleVarmodLow = 0xA857BC78,
		BullpupRifleMk2Camo = 0xAE4055B7,
		BullpupRifleMk2Camo02 = 0xB905ED6B,
		BullpupRifleMk2Camo03 = 0xA6C448E8,
		BullpupRifleMk2Camo04 = 0x9486246C,
		BullpupRifleMk2Camo05 = 0x8A390FD2,
		BullpupRifleMk2Camo06 = 0x2337FC5,
		BullpupRifleMk2Camo07 = 0xEFFFDB5E,
		BullpupRifleMk2Camo08 = 0xDDBDB6DA,
		BullpupRifleMk2Camo09 = 0xCB631225,
		BullpupRifleMk2Camo10 = 0xA87D541E,
		BullpupRifleMk2CamoIndependence01 = 0xC5E9AE52,
		BullpupRifleMk2Clip01 = 0x18929DA,
		BullpupRifleMk2Clip02 = 0xEFB00628,
		BullpupRifleMk2ClipArmorPiercing = 0xFAA7F5ED,
		BullpupRifleMk2ClipFMJ = 0x43621710,
		BullpupRifleMk2ClipIncendiary = 0xA99CF95A,
		BullpupRifleMk2ClipTracer = 0x822060A9,
		BullpupShotgunClip01 = 0xC94E550E,
		CarbineRifleClip01 = 0x9FBE33EC,
		CarbineRifleClip02 = 0x91109691,
		CarbineRifleClip03 = 0xBA62E935,
		CarbineRifleVarmodLuxe = 0xD89B9658,
		CarbineRifleMk2Camo = 0x4BDD6F16,
		CarbineRifleMk2Camo02 = 0x406A7908,
		CarbineRifleMk2Camo03 = 0x2F3856A4,
		CarbineRifleMk2Camo04 = 0xE50C424D,
		CarbineRifleMk2Camo05 = 0xD37D1F2F,
		CarbineRifleMk2Camo06 = 0x86268483,
		CarbineRifleMk2Camo07 = 0xF420E076,
		CarbineRifleMk2Camo08 = 0xAAE14DF8,
		CarbineRifleMk2Camo09 = 0x9893A95D,
		CarbineRifleMk2Camo10 = 0x6B13CD3E,
		CarbineRifleMk2CamoIndependence01 = 0xDA55CD3F,
		CarbineRifleMk2Clip01 = 0x4C7A391E,
		CarbineRifleMk2Clip02 = 0x5DD5DBD5,
		CarbineRifleMk2ClipArmorPiercing = 0x255D5D57,
		CarbineRifleMk2ClipFMJ = 0x44032F11,
		CarbineRifleMk2ClipIncendiary = 0x3D25C2A7,
		CarbineRifleMk2ClipTracer = 0x1757F566,
		CeramicPistolClip01 = 0x54D41361,
		CeramicPistolClip02 = 0x81786CA9,
		CeramicPistolSupp = 0x9307D6FA,
		CombatMGClip01 = 0xE1FFB34A,
		CombatMGClip02 = 0xD6C59CD6,
		CombatMGVarmodLowrider = 0x92FECCDD,
		CombatMGMk2Camo = 0x4A768CB5,
		CombatMGMk2Camo02 = 0xCCE06BBD,
		CombatMGMk2Camo03 = 0xBE94CF26,
		CombatMGMk2Camo04 = 0x7609BE11,
		CombatMGMk2Camo05 = 0x48AF6351,
		CombatMGMk2Camo06 = 0x9186750A,
		CombatMGMk2Camo07 = 0x84555AA8,
		CombatMGMk2Camo08 = 0x1B4C088B,
		CombatMGMk2Camo09 = 0xE046DFC,
		CombatMGMk2Camo10 = 0x28B536E,
		CombatMGMk2CamoIndependence01 = 0xD703C94D,
		CombatMGMk2Clip01 = 0x492B257C,
		CombatMGMk2Clip02 = 0x17DF42E9,
		CombatMGMk2ClipArmorPiercing = 0x29882423,
		CombatMGMk2ClipFMJ = 0x57EF1CC8,
		CombatMGMk2ClipIncendiary = 0xC326BDBA,
		CombatMGMk2ClipTracer = 0xF6649745,
		CombatPDWClip01 = 0x4317F19E,
		CombatPDWClip02 = 0x334A5203,
		CombatPDWClip03 = 0x6EB8C8DB,
		CombatPistolClip01 = 0x721B079,
		CombatPistolClip02 = 0xD67B4F2D,
		CombatPistolVarmodLowrider = 0xC6654D72,
		CombatShotgunClip01 = 0xC6153655,
		CompactGrenadeLauncherClip01 = 0x49A3CF0C,
		CompactRifleClip01 = 0x513F0A63,
		CompactRifleClip02 = 0x59FF9BF8,
		CompactRifleClip03 = 0xC607740E,
		DBShotgunClip01 = 0x29EA741E,
		DoubleActionClip01 = 0x4F312CC1,
		FireworkClip01 = 0xE4E4C28D,
		FlareGunClip01 = 0x93E9BD99,
		FlashlightLight = 0xDDB7390F,
		GrenadeLauncherClip01 = 0x11AE5C97,
		GunrunMk2Upgrade = 0x60BD749C,
		GusenbergClip01 = 0x1CE5A6A5,
		GusenbergClip02 = 0xEAC8C270,
		HeavyPistolClip01 = 0xD4A969A,
		HeavyPistolClip02 = 0x64F9C62B,
		HeavyPistolVarmodLuxe = 0x7A6A7B7B,
		HeavyShotgunClip01 = 0x324F2D5F,
		HeavyShotgunClip02 = 0x971CF6FD,
		HeavyShotgunClip03 = 0x88C7DA53,
		HeavySniperClip01 = 0x476F52F4,
		HeavySniperMk2Camo = 0xF8337D02,
		HeavySniperMk2Camo02 = 0xC5BEDD65,
		HeavySniperMk2Camo03 = 0xE9712475,
		HeavySniperMk2Camo04 = 0x13AA78E7,
		HeavySniperMk2Camo05 = 0x26591E50,
		HeavySniperMk2Camo06 = 0x302731EC,
		HeavySniperMk2Camo07 = 0xAC722A78,
		HeavySniperMk2Camo08 = 0xBEA4CEDD,
		HeavySniperMk2Camo09 = 0xCD776C82,
		HeavySniperMk2Camo10 = 0xABC5ACC7,
		HeavySniperMk2CamoIndependence01 = 0x6C32D2EB,
		HeavySniperMk2Clip01 = 0xFA1E1A28,
		HeavySniperMk2Clip02 = 0x2CD8FF9D,
		HeavySniperMk2ClipArmorPiercing = 0xF835D6D4,
		HeavySniperMk2ClipExplosive = 0x89EBDAA7,
		HeavySniperMk2ClipFMJ = 0x3BE948F6,
		HeavySniperMk2ClipIncendiary = 0xEC0F617,
		HomingLauncherClip01 = 0xF8132D3F,
		KnuckleVarmodBallas = 0xEED9FD63,
		KnuckleVarmodBase = 0xF3462F33,
		KnuckleVarmodDiamond = 0x9761D9DC,
		KnuckleVarmodDollar = 0x50910C31,
		KnuckleVarmodHate = 0x7DECFE30,
		KnuckleVarmodKing = 0xE28BABEF,
		KnuckleVarmodLove = 0x3F4E8AA6,
		KnuckleVarmodPimp = 0xC613F685,
		KnuckleVarmodPlayer = 0x8B808BB,
		KnuckleVarmodVagos = 0x7AF3F785,
		MachinePistolClip01 = 0x476E85FF,
		MachinePistolClip02 = 0xB92C6979,
		MachinePistolClip03 = 0xA9E9CAF4,
		MarksmanPistolClip01 = 0xCB9E41ED,
		MarksmanRifleClip01 = 0xD83B4141,
		MarksmanRifleClip02 = 0xCCFD2AC5,
		MarksmanRifleVarmodLuxe = 0x161E9241,
		MarksmanRifleMk2Camo = 0x9094FBA0,
		MarksmanRifleMk2Camo02 = 0x7320F4B2,
		MarksmanRifleMk2Camo03 = 0x60CF500F,
		MarksmanRifleMk2Camo04 = 0xFE668B3F,
		MarksmanRifleMk2Camo05 = 0xF3757559,
		MarksmanRifleMk2Camo06 = 0x193B40E8,
		MarksmanRifleMk2Camo07 = 0x107D2F6C,
		MarksmanRifleMk2Camo08 = 0xC4E91841,
		MarksmanRifleMk2Camo09 = 0x9BB1C5D3,
		MarksmanRifleMk2Camo10 = 0x3B61040B,
		MarksmanRifleMk2CamoIndependence01 = 0xB7A316DA,
		MarksmanRifleMk2Clip01 = 0x94E12DCE,
		MarksmanRifleMk2Clip02 = 0xE6CFD1AA,
		MarksmanRifleMk2ClipArmorPiercing = 0xF46FD079,
		MarksmanRifleMk2ClipFMJ = 0xE14A9ED3,
		MarksmanRifleMk2ClipIncendiary = 0x6DD7A86E,
		MarksmanRifleMk2ClipTracer = 0xD77A22D2,
		MicroSMGClip01 = 0xCB48AEF0,
		MicroSMGClip02 = 0x10E6BA2B,
		MicroSMGVarmodLuxe = 0x487AAE09,
		MGClip01 = 0xF434EF84,
		MGClip02 = 0x82158B47,
		MGVarmodLowrider = 0xD6DABABE,
		MilitaryRifleClip01 = 0x2D46D83B,
		MilitaryRifleClip02 = 0x684ACE42,
		MilitaryRifleSight01 = 0x6B82F395,
		MinigunClip01 = 0xC8DE6F06,
		MiniSMGClip01 = 0x84C8B2D3,
		MiniSMGClip02 = 0x937ED0B7,
		MusketClip01 = 0x4ED2073F,
		NavyRevolverClip01 = 0x985EC267,
		PericoPistolClip01 = 0xAB276E49,
		Pistol50Clip01 = 0x2297BE19,
		Pistol50Clip02 = 0xD9D3AC92,
		Pistol50VarmodLuxe = 0x77B8AB2F,
		PistolClip01 = 0xFED0FD71,
		PistolClip02 = 0xED265A1C,
		PistolVarmodLuxe = 0xD7391086,
		PistolMk2Camo = 0x5C6C749C,
		PistolMk2Camo02 = 0x15F7A390,
		PistolMk2Camo03 = 0x968E24DB,
		PistolMk2Camo04 = 0x17BFA99,
		PistolMk2Camo05 = 0xF2685C72,
		PistolMk2Camo06 = 0xDD2231E6,
		PistolMk2Camo07 = 0xBB43EE76,
		PistolMk2Camo08 = 0x4D901310,
		PistolMk2Camo09 = 0x5F31B653,
		PistolMk2Camo10 = 0x697E19A0,
		PistolMk2CamoIndependence01 = 0x930CB951,
		PistolMk2CamoSlide = 0xB4FC92B0,
		PistolMk2Camo02Slide = 0x1A1F1260,
		PistolMk2Camo03Slide = 0xE4E00B70,
		PistolMk2Camo04Slide = 0x2C298B2B,
		PistolMk2Camo05Slide = 0xDFB79725,
		PistolMk2Camo06Slide = 0x6BD7228C,
		PistolMk2Camo07Slide = 0x9DDBCF8C,
		PistolMk2Camo08Slide = 0xB319A52C,
		PistolMk2Camo09Slide = 0xC6836E12,
		PistolMk2Camo10Slide = 0x43B1B173,
		PistolMk2CamoIndependence01Slide = 0x4ABDA3FA,
		PistolMk2Clip01 = 0x94F42D62,
		PistolMk2Clip02 = 0x5ED6C128,
		PistolMk2ClipFMJ = 0x4F37DF2A,
		PistolMk2ClipHollowPoint = 0x85FEA109,
		PistolMk2ClipIncendiary = 0x2BBD7A3A,
		PistolMk2ClipTracer = 0x25CAAEAF,
		PoliceTorchFlashlight = 0xC5A30FED,
		PumpShotgunClip01 = 0xD16F1438,
		PumpShotgunVarmodLowrider = 0xA2D79DDB,
		PumpShotgunMk2Camo = 0xE3BD9E44,
		PumpShotgunMk2Camo02 = 0x17148F9B,
		PumpShotgunMk2Camo03 = 0x24D22B16,
		PumpShotgunMk2Camo04 = 0xF2BEC6F0,
		PumpShotgunMk2Camo05 = 0x85627D,
		PumpShotgunMk2Camo06 = 0xDC2919C5,
		PumpShotgunMk2Camo07 = 0xE184247B,
		PumpShotgunMk2Camo08 = 0xD8EF9356,
		PumpShotgunMk2Camo09 = 0xEF29BFCA,
		PumpShotgunMk2Camo10 = 0x67AEB165,
		PumpShotgunMk2CamoIndependence01 = 0x46411A1D,
		PumpShotgunMk2Clip01 = 0xCD940141,
		PumpShotgunMk2ClipArmorPiercing = 0x4E65B425,
		PumpShotgunMk2ClipExplosive = 0x3BE4465D,
		PumpShotgunMk2ClipHollowPoint = 0xE9582927,
		PumpShotgunMk2ClipIncendiary = 0x9F8A1BF5,
		RailgunClip01 = 0x384F3E8,
		RevolverClip01 = 0xE9867CE3,
		RevolverVarmodBoss = 0x16EE3040,
		RevolverVarmodGoon = 0x9493B80D,
		RevolverMk2Camo = 0xC03FED9F,
		RevolverMk2Camo02 = 0xB5DE24,
		RevolverMk2Camo03 = 0xA7FF1B8,
		RevolverMk2Camo04 = 0xF2E24289,
		RevolverMk2Camo05 = 0x11317F27,
		RevolverMk2Camo06 = 0x17C30C42,
		RevolverMk2Camo07 = 0x257927AE,
		RevolverMk2Camo08 = 0x37304B1C,
		RevolverMk2Camo09 = 0x48DAEE71,
		RevolverMk2Camo10 = 0x20ED9B5B,
		RevolverMk2CamoIndependence01 = 0xD951E867,
		RevolverMk2Clip01 = 0xBA23D8BE,
		RevolverMk2ClipFMJ = 0xDC8BA3F,
		RevolverMk2ClipHollowPoint = 0x10F42E8F,
		RevolverMk2ClipIncendiary = 0xEFBF25,
		RevolverMk2ClipTracer = 0xC6D8E476,
		RPGClip01 = 0x4EA573B3,
		SawnoffShotgunClip01 = 0xC7D62225,
		SawnoffShotgunVarmodLuxe = 0x85A64DF9,
		SMGClip01 = 0x26574997,
		SMGClip02 = 0x350966FB,
		SMGClip03 = 0x79C77076,
		SMGVarmodLuxe = 0x27872C90,
		SMGMk2Camo = 0xC4979067,
		SMGMk2Camo02 = 0x3815A945,
		SMGMk2Camo03 = 0x4B4B4FB0,
		SMGMk2Camo04 = 0xEC729200,
		SMGMk2Camo05 = 0x48F64B22,
		SMGMk2Camo06 = 0x35992468,
		SMGMk2Camo07 = 0x24B782A5,
		SMGMk2Camo08 = 0xA2E67F01,
		SMGMk2Camo09 = 0x2218FD68,
		SMGMk2Camo10 = 0x45C5C3C5,
		SMGMk2CamoIndependence01 = 0x399D558F,
		SMGMk2Clip01 = 0x4C24806E,
		SMGMk2Clip02 = 0xB9835B2E,
		SMGMk2ClipFMJ = 0xB5A715F,
		SMGMk2ClipHollowPoint = 0x3A1BD6FA,
		SMGMk2ClipIncendiary = 0xD99222E5,
		SMGMk2ClipTracer = 0x7FEA36EC,
		SniperRifleClip01 = 0x9BC64089,
		SniperRifleVarmodLuxe = 0x4032B5E7,
		SNSPistolClip01 = 0xF8802ED9,
		SNSPistolClip02 = 0x7B0033B3,
		SNSPistolVarmodLowrider = 0x8033ECAF,
		SNSPistolMk2Camo = 0xF7BEEDD,
		SNSPistolMk2Camo02 = 0x8A612EF6,
		SNSPistolMk2Camo03 = 0x76FA8829,
		SNSPistolMk2Camo04 = 0xA93C6CAC,
		SNSPistolMk2Camo05 = 0x9C905354,
		SNSPistolMk2Camo06 = 0x4DFA3621,
		SNSPistolMk2Camo07 = 0x42E91FFF,
		SNSPistolMk2Camo08 = 0x54A8437D,
		SNSPistolMk2Camo09 = 0x68C2746,
		SNSPistolMk2Camo10 = 0x2366E467,
		SNSPistolMk2CamoIndependence01 = 0x441882E6,
		SNSPistolMk2CamoSlide = 0xE7EE68EA,
		SNSPistolMk2Camo02Slide = 0x29366D21,
		SNSPistolMk2Camo03Slide = 0x3ADE514B,
		SNSPistolMk2Camo04Slide = 0xE64513E9,
		SNSPistolMk2Camo05Slide = 0xCD7AEB9A,
		SNSPistolMk2Camo06Slide = 0xFA7B27A6,
		SNSPistolMk2Camo07Slide = 0xE285CA9A,
		SNSPistolMk2Camo08Slide = 0x2B904B19,
		SNSPistolMk2Camo09Slide = 0x22C24F9C,
		SNSPistolMk2Camo10Slide = 0x8D0D5ECD,
		SNSPistolMk2CamoIndependence01Slide = 0x1F07150A,
		SNSPistolMk2Clip01 = 0x1466CE6,
		SNSPistolMk2Clip02 = 0xCE8C0772,
		SNSPistolMk2ClipFMJ = 0xC111EB26,
		SNSPistolMk2ClipHollowPoint = 0x8D107402,
		SNSPistolMk2ClipIncendiary = 0xE6AD5F79,
		SNSPistolMk2ClipTracer = 0x902DA26E,
		SpecialCarbineClip01 = 0xC6C7E581,
		SpecialCarbineClip02 = 0x7C8BD10E,
		SpecialCarbineClip03 = 0x6B59AEAA,
		SpecialCarbineVarmodLowrider = 0x730154F2,
		SpecialCarbineMk2Camo = 0xD40BB53B,
		SpecialCarbineMk2Camo02 = 0x431B238B,
		SpecialCarbineMk2Camo03 = 0x34CF86F4,
		SpecialCarbineMk2Camo04 = 0xB4C306DD,
		SpecialCarbineMk2Camo05 = 0xEE677A25,
		SpecialCarbineMk2Camo06 = 0xDF90DC78,
		SpecialCarbineMk2Camo07 = 0xA4C31EE,
		SpecialCarbineMk2Camo08 = 0x89CFB0F7,
		SpecialCarbineMk2Camo09 = 0x7B82145C,
		SpecialCarbineMk2Camo10 = 0x899CAF75,
		SpecialCarbineMk2CamoIndependence01 = 0x5218C819,
		SpecialCarbineMk2Clip01 = 0x16C69281,
		SpecialCarbineMk2Clip02 = 0xDE1FA12C,
		SpecialCarbineMk2ClipArmorPiercing = 0x51351635,
		SpecialCarbineMk2ClipFMJ = 0x503DEA90,
		SpecialCarbineMk2ClipIncendiary = 0xDE011286,
		SpecialCarbineMk2ClipTracer = 0x8765C68A,
		SweeperShotgunClip01 = 0xA19D08E,
		SwitchbladeVarmodBase = 0x9137A500,
		SwitchbladeVarmodVar1 = 0x5B3E7DB6,
		SwitchbladeVarmodVar2 = 0xE7939662,
		VintagePistolClip01 = 0x45A3B6BB,
		VintagePistolClip02 = 0x33BA12E8,
		UpNAtomizerVarmodXmas18 = 0xD7DBF707,
		Invalid = 0xFFFFFFFF,
	}
