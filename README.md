# Liens utiles

Documentation ESX : https://esx-framework.github.io/

Ambient Voices :
    http://www.kronzky.info/fivemwiki/index.php/Ambient_Voices

Animations :
    https://gtahash.ru/animations/ ou 
    https://wiki.rage.mp/index.php?title=Animations ou 
    https://wiki.gtanet.work/index.php?title=Animations ou 
    Animations List : https://docs.ragepluginhook.net/html/62951c37-a440-478c-b389-c471230ddfc5.htm ou 
    https://alexguirre.github.io/animations-list/ ou 
    https://wiki.altv.mp/wiki/GTA:Ped_Animations

Blips :
    https://docs.fivem.net/docs/game-references/blips/ ou 
    https://wiki.rage.mp/index.php?title=Blips ou 
    https://wiki.gtanet.work/index.php?title=Blips ou 
    http://www.kronzky.info/fivemwiki/index.php/Blips ou 
    https://wiki.altv.mp/wiki/GTA:Blips

Causes Of Death :
    https://wiki.rage.mp/index.php?title=Causes_of_death

Checkpoints :
    https://docs.fivem.net/docs/game-references/checkpoints/ ou 
    https://wiki.altv.mp/wiki/GTA:CheckpointTypes

Damage Packs :
    http://www.kronzky.info/fivemwiki/index.php/Damage_Packs

Data files :
    https://docs.fivem.net/docs/game-references/data-files/ ou 
    https://wiki.altv.mp/wiki/GTA:Data_files

Door models and positions :
    https://wiki.gtanet.work/index.php?title=Doors

Draw Text Colors :
    A simple tool allowing for basic "rendering" of text colors using the ~(...)~ color codes in functions like DrawText() in GTA V.
    https://vespura.com/fivem/colors/

Driving Styles :
    http://www.kronzky.info/fivemwiki/index.php/Driving_Styles
    Driving Style Calculator : https://vespura.com/fivem/drivingstyle/

Entity Types :
    https://wiki.gtanet.work/index.php?title=Entity_Types

Explosions :
    https://wiki.rage.mp/index.php?title=Explosions ou 
    https://wiki.gtanet.work/index.php?title=Explosions

Face Features : 
    http://www.kronzky.info/fivemwiki/index.php/Face_Features

Fonts and Colors :
    https://wiki.rage.mp/index.php?title=Fonts_and_Colors ou 
    https://wiki.gtanet.work/index.php?title=Fonts

Groups :
    http://www.kronzky.info/fivemwiki/index.php/Groups

HUD Components :
        https://wiki.rage.mp/index.php?title=HUD_Components ou 
        http://www.kronzky.info/fivemwiki/index.php/HUD_Components ou 
        https://wiki.altv.mp/wiki/GTA:HUD_Components

HUD Color Indices :
        http://www.kronzky.info/fivemwiki/index.php/HUD_Color_Indices

Interior Props :
    https://wiki.rage.mp/index.php?title=Interior_Props

Interiors and Locations :
    https://wiki.rage.mp/index.php?title=Interiors_and_Locations ou 
    https://wiki.gtanet.work/index.php?title=Online_Interiors_and_locations ou 
    https://wiki.altv.mp/wiki/GTA:Online_Interiors_and_Locations

IPLs :
    https://wiki.rage.mp/index.php?title=IPLs ou 
    https://wiki.gtanet.work/index.php?title=IPLs ou 
    https://forge.plebmasters.de/ipls

Markers :
    https://wiki.gtanet.work/index.php?title=Marker ou 
    https://wiki.rage.mp/index.php?title=Markers ou 
    http://www.kronzky.info/fivemwiki/index.php/Marker_Types ou 
    https://wiki.altv.mp/wiki/GTA:Markers

Materials :
    http://www.kronzky.info/fivemwiki/index.php/Materials

MLOs :
    https://forge.plebmasters.de/mlos

Moods :
    http://www.kronzky.info/fivemwiki/index.php/Moods

Movement Clipsets :
    http://www.kronzky.info/fivemwiki/index.php/Movement_Clipsets

Music Events :
    http://www.kronzky.info/fivemwiki/index.php/Music_Events

Natives :
    https://docs.fivem.net/natives/ ou 
    http://dev-c.com/nativedb/func/ ou 
    https://cdn.rage.mp/public/natives/

Notification Pictures :
    https://wiki.gtanet.work/index.php?title=Notification_Pictures ou 
    http://www.kronzky.info/fivemwiki/index.php/Notification_Pictures

Objets :
    https://cdn.rage.mp/public/odb/# (Pas le meilleur site) ou 
    https://gtahash.ru/ (Objects disponible par catégorie) ou 
    http://www.kronzky.info/fivemwiki/index.php/Props ou 
    https://forge.plebmasters.de/objects ou 
    https://gta-objects.xyz/objects ou 
    https://vespura.com/fivem/objects/

Particles FX :
    https://wiki.rage.mp/index.php?title=Particles_Effects ou 
    http://www.kronzky.info/fivemwiki/index.php/Particles ou 
    https://gtaforums.com/topic/801286-gta-v-particle-effects/?tab=comments#comment-1067603072 ou 
    https://vespura.com/fivem/particle-list/

PED Body Parts :
    https://wiki.altv.mp/wiki/GTA:Ped_BodyParts

PED Bones :
    https://wiki.gtanet.work/index.php?title=Bones ou 
    https://wiki.rage.mp/index.php?title=Bones ou 
    http://www.kronzky.info/fivemwiki/index.php/Bones ou 
    https://wiki.altv.mp/wiki/GTA:Ped_Bones

PED Clothes :
    https://wiki.rage.mp/index.php?title=Clothes ou 
    https://wiki.gtanet.work/index.php?title=Character_Components ou 
    https://forge.plebmasters.de/clothes

PED Models :
    https://docs.fivem.net/docs/game-references/ped-models/ ou 
    https://wiki.rage.mp/index.php?title=Peds ou 
    https://wiki.gtanet.work/index.php?title=Peds ou 
    http://www.kronzky.info/fivemwiki/index.php/Ped_Models ou
    https://forge.plebmasters.de/peds ou 
    https://wiki.altv.mp/wiki/GTA:Ped_Models

PED Types :
    http://www.kronzky.info/fivemwiki/index.php/Ped_Types ou 

PED Config Flags :
    https://wiki.altv.mp/wiki/GTA:Ped_Config_Flags

Pickup :
    https://wiki.rage.mp/index.php?title=Pickups ou 
    https://wiki.gtanet.work/index.php?title=Pickups

Player Properties :
    https://wiki.gtanet.work/index.php?title=Client

Radio Stations :
    http://www.kronzky.info/fivemwiki/index.php/Radio_Stations

Screen Effects :
    http://www.kronzky.info/fivemwiki/index.php/Category:Reference_Lists

Shocking Events :
    http://www.kronzky.info/fivemwiki/index.php/Category:Reference_Lists

Skins :
    https://gtahash.ru/skins/

Sounds :    Utiliser la native playSoundFrontend https://docs.fivem.net/natives/?_0x67C540AA08E4A6F5 et setAudioFlag https://docs.fivem.net/natives/?_0xB9EFD5C25018725A
    
    https://wiki.rage.mp/index.php?title=Sounds ou 
    https://wiki.gtanet.work/index.php?title=FrontEndSoundlist ou 

    http://www.kronzky.info/fivemwiki/index.php/PlaySound_Examples ou 
    http://www.kronzky.info/fivemwiki/index.php/PlaySoundFromCoord_Examples ou 
    http://www.kronzky.info/fivemwiki/index.php/PlaySoundFromEntity_Examples ou 
    http://www.kronzky.info/fivemwiki/index.php/PlaySoundFrontend_Examples

Tattoo Zones :
    http://www.kronzky.info/fivemwiki/index.php/Category:Reference_Lists#:~:text=T-,Tattoo%20Zones,-Text%20Colors
    https://forge.plebmasters.de/decorations

Text Colors :
    http://www.kronzky.info/fivemwiki/index.php/Text_Colors

Time and Date :
    https://wiki.altv.mp/wiki/GTA:Time_and_Date

Keys :
    https://docs.fivem.net/docs/game-references/controls/ ou 
    https://wiki.rage.mp/index.php?title=Controls ou 
    https://wiki.gtanet.work/index.php?title=Game_Controls ou 
    https://wiki.altv.mp/wiki/GTA:Controls

Vehicles Bones :
        https://wiki.rage.mp/index.php?title=Vehicle_Bones ou 
        https://wiki.altv.mp/wiki/GTA:Vehicle_Bones

Vehicles Classes :
        https://wiki.rage.mp/index.php?title=Vehicle_Classes ou 
        https://wiki.gtanet.work/index.php?title=Vehicle_Classes ou 
        https://wiki.altv.mp/wiki/GTA:Vehicle_Classes

Vehicles Colors :
        https://wiki.rage.mp/index.php?title=Vehicle_Colors ou 
        https://wiki.gtanet.work/index.php?title=Vehicle_Colors ou 
        https://wiki.altv.mp/wiki/GTA:Vehicle_Colors

Vehicles Models :
        https://gtahash.ru/car/ ou 
        https://wiki.rage.mp/index.php?title=Vehicles ou 
        https://wiki.gtanet.work/index.php?title=Vehicle_Models ou 
        http://www.kronzky.info/fivemwiki/index.php/Vehicle_Models ou 
        https://forge.plebmasters.de/vehicles
        https://gta-objects.xyz/vehicles ou 
        https://wiki.altv.mp/wiki/GTA:Vehicle_Models ou 
        https://wiki.altv.mp/wiki/GTA:Vehicle_Models

Vehicles Mods :
        https://wiki.rage.mp/index.php?title=Vehicle_Mods ou 
        https://wiki.gtanet.work/index.php?title=Vehicle_Mods ou
        https://forge.plebmasters.de/vehiclemods ou 
        https://wiki.altv.mp/wiki/GTA:Vehicle_Mods

Vehicles Flags :
        https://wiki.rage.mp/index.php?title=Vehicle_Flags ou 
        https://wiki.gtanet.work/index.php?title=Vehicle_Flags ou 
        https://wiki.altv.mp/wiki/GTA:Vehicle_Flags

Vehicles Components :
        http://www.kronzky.info/fivemwiki/index.php/Vehicle_Components

Vehicle Types : 
        https://wiki.altv.mp/wiki/GTA:Vehicle_Types

Warning buttons :
    A simple flags calculator, used to calculate the exact value for all the buttons that you want to appear on warning/alert screens in GTA V. You'll know what it is if you're looking for this.
    https://vespura.com/fivem/warning-buttons/

Weapons Components :
        https://wiki.rage.mp/index.php?title=Weapons_Components ou 
        https://wiki.gtanet.work/index.php?title=Weapons_Components ou 
        https://wiki.altv.mp/wiki/GTA:Weapon_Components

Weapons Models :
        https://gtahash.ru/weapons/ ou 
        https://wiki.rage.mp/index.php?title=Weapons ou 
        https://wiki.gtanet.work/index.php?title=Weapons_Models ou 
        http://www.kronzky.info/fivemwiki/index.php/Weapons ou 
        https://forge.plebmasters.de/weapons ou 
        https://gta-objects.xyz/weapons ou 
        https://wiki.altv.mp/wiki/GTA:Weapon_Models

Weapons Tints :
        https://wiki.rage.mp/index.php?title=Weapons_Tints ou 
        https://wiki.gtanet.work/index.php?title=Weapons_Tints ou 
        https://wiki.altv.mp/wiki/GTA:Weapon_Tints

Waypoint Recording Names :
    http://www.kronzky.info/fivemwiki/index.php/WaypointRecordingNames

Waypoint Recording Points :
    http://www.kronzky.info/fivemwiki/index.php/WaypointRecordingPoints

Weather :
    https://wiki.rage.mp/index.php?title=Weather ou 
    https://wiki.gtanet.work/index.php?title=Weather ou 
    http://www.kronzky.info/fivemwiki/index.php/Weather_Types ou 
    https://wiki.altv.mp/wiki/GTA:Weather
