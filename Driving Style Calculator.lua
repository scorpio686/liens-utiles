https://vespura.com/fivem/drivingstyle/

Code Examples (Lua)

-- Setup variables
local ped = PlayerPedId()
local vehicle = GetVehiclePedIsIn(ped, false)
local x = 40.0
local y = -492.0
local z = 20.0
local stopRange = 8.0
local speed = 20.0 -- averages around 45 mph top speed.
local drivingStyle = 4982588

-- Useful functions to make the ped perform better while driving.
SetDriverAbility(ped, 1.0)        -- values between 0.0 and 1.0 are allowed.
SetDriverAggressiveness(ped, 0.0) -- values between 0.0 and 1.0 are allowed.

-- Example 1
-- Give the player a wander driving task.
TaskVehicleDriveWander(ped, vehicle, speed, drivingStyle)

-- Example 2
-- Manually set/override the driving style (after giving the ped a driving task).
SetDriveTaskDrivingStyle(ped, drivingStyle)

-- Example 3
-- Drive to a location (far away).
TaskVehicleDriveToCoordLongrange(ped, vehicle, x, y, z, speed, drivingStyle, stopRange);


